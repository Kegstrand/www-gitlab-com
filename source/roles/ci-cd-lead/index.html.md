---
layout: markdown_page
title: "CI/CD Lead"
---

This page has been deprecated and moved to the [Developer](/roles/developer/#cicd) job description.
