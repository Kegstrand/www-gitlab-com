---
layout: job_page
title: "Backend Developer, Edge"
---

This page has been deprecated and moved to the [Developer](/roles/developer/#edge) job description.
